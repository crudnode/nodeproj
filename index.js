const express = require('express')
const cors = require('cors')
const student = require('./student')
const app = express()

app.use(cors())

app.get('/', (req, res, next) =>{
  res.json({msg: 'This is CORS-enabled for all origins!'})
})


// app.use((req,res,next)=>{
//     res.setHeader('Access-Control-Allow-Origin',"*");
//     //line above  will only make GET possible in case of CORS issue

//     res.setHeader('Access-Control-Allow-Methods',"*");
//     //line abovewill make GET/POST/PUT/DELETE possible in case of CORS 
    
//     res.setHeader('Access-Control-Allow-Headers',"*");
//     //line abovewill make getting authorization etc headers possible in cors
//     next();
// })

app.use('/student',student)

app.get('/',(req,res)=>{
    res.send("Welcome to Server")
})

app.listen(3000, ()=>{
    console.log("Server started on port:7000")
})